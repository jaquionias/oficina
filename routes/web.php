<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/** Página de listagem dos orçamentos */
Route::get('/', 'OrcamentoController@index')->name('orcamento.index');
/** Rota de filtrar a listagem */
Route::match(['post', 'get'], '/filtro', 'OrcamentoController@filter')->name('orcamento.filter');

/** Pagina de cadastrar orçamentos */
Route::get('/cadastrar', 'OrcamentoController@create')->name('orcamento.create');
/** Rota de salvar orçamentos*/
Route::post('/cadastrar/save', 'OrcamentoController@store')->name('orcamento.store');

/** Pagina de editar orçamentos */
Route::get('/editar/{id}', 'OrcamentoController@edit')->name('orcamento.edit');
/** Rota de editar orçamentos*/
Route::put('/editar/update/{id}', 'OrcamentoController@update')->name('orcamento.update');

/** Rota de deletar */
Route::get('/destroy/{id}', 'OrcamentoController@destroy')->name('orcamento.destroy');
