## Setup do projeto
##### Na pasta do projeto
```
composer install
```

##### Configurar .env

```
cp .env.example .env
```

##### Gerar chave da aplicação

```
php artisan key:generate
```

## Criar um banco de dados Mysql localmente

SETAR AS CREDENCIAIS DO BANCO DE DADOS NO ARQUIVO .env;

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=nome_do_BD
DB_USERNAME=usuario_do_BD
DB_PASSWORD=senha_do_BD
```

### Rodar as migrations para criação da estrutura do banco

```
php artisan migrate
```

### Iniciar projeto
```
php artisan serve
```
