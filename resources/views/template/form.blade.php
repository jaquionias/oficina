@extends('template.master')


@section('content')
    <div class="container">
        <div class="row mt-5 mb-5">
            <div class="col-12" style="display: flex; flex-direction: row; align-items: center; justify-content: center;">
                <img class="logo" style="margin-right: 20px" width="100" src="{{asset('./img/logo.png')}}" alt="">
                @if(isset($orcamento->id))
                    <h1>Edição de Orçamentos </h1>
                @else
                    <h1>Cadastro de Orçamentos </h1>
                @endif

            </div>
        </div>
        @if($errors->all())
            @foreach($errors->all() as $error)
                <div class="alert alert-danger" role="alert">
                    {{$error}}
                </div>
            @endforeach
        @endif
        @if(session()->exists('message'))
            <div class="alert alert-success" role="alert">
                {{session()->get('message')}}
            </div>
        @endif
        <form action="{{isset($orcamento->id) ? route('orcamento.update', ['id' => $orcamento->id]) : route('orcamento.store')}}" method="post">
            @csrf
            @if(isset($orcamento->id))
                @method('PUT')
            @endif

            <div class="form-group">
                <div class="row">
                    <div class="col-lg-4 col-sm-12">
                        <label for="cliente">Cliente</label>
                        <select name="cliente" id="cliente" class="form-control {{$errors->has('cliente') ? 'is-invalid' : ''}}">
                            <option value="">--Selecione o cliente--</option>
                            <option {{old('cliente') == 'Jack Ferraz' ? 'selected' : (($orcamento->cliente ?? '') == 'Jack Ferraz' ? 'selected' : '')}} value="Jack Ferraz">Jack Ferraz</option>
                            <option {{old('cliente') == 'Luiza Lopes' ? 'selected' : (($orcamento->cliente ?? '') == 'Luiza Lopes' ? 'selected' : '')}} value="Luiza Lopes">Luiza Lopes</option>
                        </select>
                        @if($errors->has('cliente'))
                            <div class="invalid-feedback">
                                {{$errors->first('cliente')}}
                            </div>
                        @endif
                    </div>
                    <div class="col-lg-4 col-sm-12">
                        <label for="vendedor">Vendedor</label>
                        <select name="vendedor" id="vendedor" class="form-control {{$errors->has('vendedor') ? 'is-invalid' : ''}}">
                            <option value="">--Selecione o cliente--</option>
                            <option {{old('vendedor') == 'Luiz Otavio' ? 'selected' : (($orcamento->vendedor ?? '') == 'Luiz Otavio' ? 'selected' : '')}} value="Luiz Otavio">Luiz Otavio</option>
                            <option {{old('vendedor') == 'Marcelo Nolasco' ? 'selected' : (($orcamento->vendedor ?? '') == 'Marcelo Nolasco' ? 'selected' : '')}} value="Marcelo Nolasco">Marcelo Nolasco</option>
                        </select>
                        @if($errors->has('vendedor'))
                            <div class="invalid-feedback">
                                {{$errors->first('vendedor')}}
                            </div>
                        @endif
                    </div>
                    <div class="col-lg-4 col-sm-12">
                        <label for="valor">Valor</label>
                        <input value="{{old('valor') ?? $orcamento->valor ?? ''}}" type="number" placeholder="Ex.: R$ 100,00" name="valor" id="valor"
                               class="form-control {{$errors->has('valor') ? 'is-invalid' : ''}}">
                        @if($errors->has('valor'))
                            <div class="invalid-feedback">
                                {{$errors->first('valor')}}
                            </div>
                        @endif
                    </div>
                    <div class="col-lg-12 mt-3">
                        <label for="descricao">Descrição</label>
                        <textarea placeholder="Descrição do orçamento" name="descricao" id="descricao" cols="30" rows="10"
                                  class="form-control  {{$errors->has('descricao') ? 'is-invalid' : ''}}">{{old('descricao') ?? $orcamento->descricao ?? ''}}</textarea>
                        @if($errors->has('descricao'))
                            <div class="invalid-feedback">
                                {{$errors->first('descricao')}}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-lg-2 mt-2">
                        <button type="submit" style="width: 100%" class="btn btn-success">Salvar</button>
                    </div>
                    <div class="col-12 col-lg-2 mt-2">
                        <a href="{{route('orcamento.index')}}" style="width: 100%"  class="btn btn-info ">Voltar</a>
                    </div>

                </div>

            </div>
        </form>

    </div>
@endsection
