@extends('template.master')

@section('content')
    <div class="container">
        <div class="row mt-5 mb-5">
            <div class="col-12" style="display: flex; flex-direction: row; align-items: center; justify-content: center;">
                <img class="logo" style="margin-right: 20px" width="100" src="{{asset('./img/logo.png')}}" alt="">
                <h1>Listagem de Orçamentos </h1>
            </div>
        </div>
        <div class="row">
            @if(session()->exists('message'))
                <div class="col-12 alert text-center alert-success" role="alert">
                    {{session()->get('message')}}
                </div>
            @endif
        </div>
        <div class="row">
            <div class="col-12 mb-2 text-right">
                <a href="{{route('orcamento.create')}}" class="btn btn-success">Novo Orçamento</a>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <form action="{{route('orcamento.filter')}}" method="post">
                    @csrf
                    <div class="form-group">
                        <div class="row mb-5">
                            <div class="col-lg-6 col-sm-12">
                                <label for="cliente">Cliente</label>
                                <select name="cliente" id="cliente" class="form-control">
                                    <option value="">--Selecione o cliente--</option>
                                    <option {{($filtros['cliente'] ?? '') == 'Jack Ferraz' ? 'selected' : ''}} value="Jack Ferraz">Jack Ferraz</option>
                                    <option {{($filtros['cliente'] ?? '') == 'Luiza Lopes' ? 'selected' : ''}} value="Luiza Lopes">Luiza Lopes</option>
                                </select>
                            </div>
                            <div class="col-lg-6 col-sm-12">
                                <label for="vendedor">Vendedor</label>
                                <select name="vendedor" id="vendedor" class="form-control">
                                    <option value="">--Selecione o cliente--</option>
                                    <option {{($filtros['vendedor'] ?? '')== 'Luiz Otavio' ? 'selected' : ''}} value="Luiz Otavio">Luiz Otavio</option>
                                    <option {{($filtros['vendedor'] ?? '') == 'Marcelo Nolasco' ? 'selected' : ''}} value="Marcelo Nolasco">Marcelo Nolasco</option>
                                </select>

                            </div>
                            <div class="col-lg-4 col-sm-12">
                                <label for="data_inicial">Data Inicial</label>
                                <input value="{{($filtros['data_inicial'] ?? '')}}" type="date" name="data_inicial" id="data_inicial"
                                       class="form-control">

                            </div>
                            <div class="col-lg-4 col-sm-12">
                                <label for="data_final">Data Final</label>
                                <input value="{{($filtros['data_final'] ?? '')}}" type="date" name="data_final" id="data_final"
                                       class="form-control">
                            </div>

                            <div class="col-12 col-lg-2 mt-4">
                                <button type="submit" style="width: 100%; margin-top: 6px" class="btn btn-info">Filtar</button>
                            </div>
                            <div class="col-12 col-lg-2 mt-4">
                                <a href="{{route('orcamento.index')}}" style="width: 100%; margin-top: 6px" class="btn btn-secondary">Limpar</a>
                            </div>
                        </div>

                    </div>
                </form>
            </div>

        </div>
        <div class="row">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Cliente</th>
                    <th scope="col">Vendedor</th>
                    <th scope="col">Data do orçamento</th>
                    <th scope="col">Ações</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orcamentos as $orcamento)
                    <tr>
                        <td>{{$orcamento->cliente}}</td>
                        <td>{{$orcamento->vendedor}}</td>
                        <td>{{$orcamento->created_at}}</td>
                        <td>
                            <a class="accordion-toggle collapsed" id="accordion{{$orcamento->id}}"
                               data-toggle="collapse" data-parent="#accordion{{$orcamento->id}}"
                               href="#collapse{{$orcamento->id}}">Ver</a>
                            <a href="{{route('orcamento.edit',['id' => $orcamento->id])}}"> | Editar</a>
                            <a href="javascript:func()" onclick="confirmacao('{{$orcamento->id}}')"> | Excluir</a>
                        </td>
                    </tr>
                    <tr class="hide-table-padding">
                        <td colspan="5" style="max-width: 100px; border:none">
                            <div class="row">
                                <div class="col-12">
                                    <div style="background: rgba(255,255,255,1);" id="collapse{{$orcamento->id}}" class="collapse in p-0">
                                        <p style="padding: 10px">
                                            <strong>Descriação: </strong>{{$orcamento->descricao}}
                                        </p>
                                        <p style="padding: 10px"><strong>Valor: </strong>R$ {{$orcamento->valor}},00
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach

                </tbody>
                <tfoot>
                {{$orcamentos}}

                </tfoot>
            </table>
        </div>
    </div>
@endsection
@section('scripts')
    <script language="Javascript">
        function confirmacao(id) {
            var resposta = confirm("Realmente deseja remover esse registro?");
            if (resposta == true) {
                window.location.href = "destroy/" + id;
            }
        }
    </script>
@endsection
