<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Orcamento extends Model
{
    protected $fillable = [
        'cliente',
        'vendedor',
        'valor',
        'descricao'
    ];

    public function getCreatedatAttribute($value)
    {
        return date('d/m/Y H:i', strtotime($value));
    }
}
