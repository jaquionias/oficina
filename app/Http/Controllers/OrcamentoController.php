<?php

namespace App\Http\Controllers;

use App\Entities\Orcamento;
use App\Http\Requests\OrcamentoRequest;
use Illuminate\Http\Request;

class OrcamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     *
     */
    public function index()
    {
        $orcamentos = Orcamento::orderBy('created_at', 'DESC')->paginate(10);
        return view('template.list', [
            'filtros' => [
                'cliente' => '',
                'vendedor' => '',
                'data_inicial' => '',
                'data_final' => '',
            ],
            'orcamentos' => $orcamentos
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return
     */
    public function create()
    {
        return view('template.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\OrcamentoRequest $request
     *
     */
    public function store(OrcamentoRequest $request)
    {
        $dados = $request->all();
        $response = Orcamento::create($dados);
        if ($response) {
            return redirect()->route('orcamento.index')->with([
                'message' => 'Orçamento cadastrado com sucesso!'
            ]);
        }
    }

    public function filter(Request $request)
    {
        $filtros = $request->all();
        $orcamentos = Orcamento::where(function ($q) use ($filtros) {
            if (isset($filtros['cliente'])) {
                $q->where('cliente', 'like', "%" . $filtros['cliente'] . "%");
            }
            if (isset($filtros['vendedor'])) {
                $q->where('vendedor', 'like', "%" . $filtros['vendedor'] . "%");
            }
            if (isset($filtros['data_inicial'])) {
                $q->where('created_at', '>=', $this->convertStringToDate($filtros['data_inicial']));
            }
            if (isset($filtros['data_final'])) {
                $q->where('created_at', '<=', ($this->convertStringToDate($filtros['data_final']) . ' 23:59:59'));
            }
        })->orderBy('created_at', 'DESC')->paginate(1000);

        return view('template.list', [
            'filtros' => $request->all(),
            'orcamentos' => $orcamentos
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     */
    public function edit($id)
    {
        $orcamento = Orcamento::where('id', $id)->first();
        return view('template.form', [
            'orcamento' => $orcamento
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\OrcamentoRequest $request
     * @param int $id
     *
     */
    public function update(OrcamentoRequest $request, $id)
    {
        $orcamento = Orcamento::where('id', $id)->first();

        if (!$orcamento->update($request->all())) {
            return redirect()->back()->withInput()->withErrors();
        }
        return redirect()->route('orcamento.edit', [
            'id' => $orcamento->id
        ])->with([
            'message' => 'Orçamento atualizado com sucesso!'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     */
    public function destroy($id)
    {
        $orcamento = Orcamento::where('id', $id)->first();
        $orcamento->delete();

        return redirect()->route('orcamento.index')->with([
            'message' => 'Orçamento deletado com sucesso!'
        ]);
    }

    public function convertStringToDate(?string $param)
    {
        if (empty($param)) {
            return null;
        }

        list($day, $month, $year) = explode('-', $param);

        return (new \DateTime($year . '-' . $month . '-' . $day))->format('Y-m-d');
    }
}
