<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrcamentoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cliente' => 'required',
            'vendedor' => 'required',
            'valor' => 'required',
            'descricao' => 'required|min:3|max:1000'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'cliente.required' => 'O campo Cliente é obrigatório!',
            'vendedor.required' => 'O campo Vendedor é obrigatório!',
            'valor.required' => 'O campo Valor é obrigatório!',
            'descricao.required' => 'O campo Descricao é obrigatório!',
        ];
    }
}
